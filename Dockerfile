# Базовый образ
FROM mambaorg/micromamba
USER root
WORKDIR /app 
COPY env.yml /app/env.yml

RUN micromamba create -f env.yml
COPY pyproject.toml /app/pyproject.toml
COPY poetry.lock /app/poetry.lock 
RUN micromamba run -n dvc_pipline poetry install --with dev\
    && micromamba run -n dvc_pipline poetry config virtualenvs.create false

import re
from pathlib import Path

import polars as pl 
import click 
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from .cli import cli
    
def text_processing(input_text: str) -> str:
    text = input_text.lower()
    text = re.sub("[0-9 \-_]+", " ", text) 
    text = re.sub("[^a-z A-Z]+"," ", text)
    text = " ".join([
        word for word in text.split() if word not in stopwords.words("english")
    ])
    return text.strip()

def lemmatizie(input_frame: pl.DataFrame) -> pl.DataFrame:
    lemmatizier = WordNetLemmatizer()
    return input_frame.with_columns(
        pl.col('corpus').map_elements(
            lambda input_list: [lemmatizier.lemmatize(token) for token in input_list]
        )
    )
def dataframe_preprocessing(data: pl.DataFrame, col_name: str) -> pl.DataFrame:
    return lemmatizie(
        data.with_columns(
            pl.col(col_name)
            .map_elements(text_processing)
            .str.split(" ")
            .alias('corpus')
        )
    )

@cli.command()
@click.argument('input_frame_path', type=Path)
@click.argument('output_frame_path', type=Path)
@click.argument('column', type=str, default='Review')
def cli_command(input_frame_path: Path, output_frame_path: Path, column: str):
    data = pl.read_csv(
        input_frame_path,
        has_header=False,
        new_columns=['Polarity', 'Title', 'Review'],
        n_rows = 1000
    )
    output_path = Path(output_frame_path)
    
    if not output_path.parent.exists():
        output_path.parent.mkdir()
        
    processed_data = dataframe_preprocessing(data, column)
    processed_data.write_parquet(output_frame_path)
    

